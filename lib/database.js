var Sequelize = require('sequelize');
var Config = require('config');
var Validation = require('validation');
var Errors = require('common-errors');
var UuidBase62 = require('uuid-base62');

Sequelize.UUID = Sequelize.CHAR(22).BINARY;

var databaseConfig = Config.get('database');

exports.init = function (modelModules, callback) {

    var sequelize = new Sequelize(databaseConfig.uri, databaseConfig.options);

    var models = {};
    var modelName = '';
    var modelModule = null;

    for (modelName in modelModules) {

        if (modelModules.hasOwnProperty(modelName) === false) {
            continue;
        }

        modelModule = modelModules[modelName];

        var schema = schemaWithValitation(modelModule.schema(Sequelize));
        var hooks = (modelModule.hooks) ? modelModule.hooks : {};

        if (schema.id && schema.id.type && schema.id.type === Sequelize.UUID) {
            hooks.beforeCreate = beforeCreateUuid(hooks.beforeCreate);
        }

        var model = sequelize.define(modelName, schema, {
            freezeTableName: true,
            classMethods: (modelModule.classMethods) ? modelModule.classMethods : {},
            instanceMethods: (modelModule.instanceMethods) ? modelModule.instanceMethods : {},
            hooks: hooks
        });

        models[modelName] = model;
        modelModule.model = model;
    }

    for (modelName in modelModules) {

        if (modelModules.hasOwnProperty(modelName) === false) {
            continue;
        }

        modelModule = modelModules[modelName];
        if (modelModule.associate) {
            modelModule.associate.apply(modelModule.model, [models]);
        }
    }

    // o método sync do sequelize serve para criar as tabelas que foram definidas pelo model no nosso BD, basta chamar uma vez na inicialização do servidor
    sequelize.sync().then(function () {

        callback(null, models);
    });
};

var beforeCreateUuid = function (callback) {

    return function (instance, options, next) {

        // Antes de criar, adicionamos no id um identificador aleatório no padrão 'uuid': https://en.wikipedia.org/wiki/Universally_unique_identifier
        // Usamos ele em base62 em vez do formato original pois é mais amigável na URL
        instance.id = UuidBase62.v4();

        if (callback) {
            return callback.apply(this, arguments);
        }

        return next();
    };
};

// Adiciona funções de validação no esquema do Sequelize. São verificações para ver se é único e se é válido de acordo com o esquema definido em Validation (assumindo que o nome dos campos do model está com o mesmo nome no Validation
var schemaWithValitation = function (schema, modelName) {

    for (var key in schema) {

        if (schema.hasOwnProperty(key) === false) {
            continue;
        }

        if (typeof schema[key].validate === 'undefined') {
            schema[key].validate = {};
        }

        if (Validation.validators[modelName] && Validation.validators[modelName][key]) {
            schema[key].validate.isValid = Validation.validationFunction(Validation.validators[modelName][key], key);
        }

        if (schema[key].unique) {
            var msg = (schema[key].uniqueMessage) ? schema[key].uniqueMessage : 'Model "' + modelName + '" key "' + key + '" must be unique';
            schema[key].validate.isUnique = isUnique(key, msg);
        }
    }

    return schema;
};

// Função de validação genérica para verificar se um campo é único na tabela. Recebe a coluna e uma mensagem de erro
var isUnique = function (col, msg) {

    var conditions = { where: {} };
    var message = (msg) ? msg : col + ' must be unique';
    var createError = function (m) {

        // Gera um erro usando common-errors
        var err = new Errors.AlreadyInUseError('Model', col);
        err.message = m;
        return err;
    };

    return function (value, next) {

        var self = this;
        this.Model.describe().then(function (schema) {

            conditions.where[col] = value;
            Object.keys(schema).filter(function (field) {

                return schema[field].primaryKey;
            }).forEach(function (pk) {

                conditions.where[pk] = { $ne: self[pk] };
            });
        }).then(function () {

            return self.Model.count(conditions).then(function (found) {

                if (found !== 0) {
                    return next(createError(message));
                }

                return next(null);
            });
        }).catch(function () {

            next(createError(message));
        });
    };
};
