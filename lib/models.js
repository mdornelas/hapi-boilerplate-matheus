// Todos os nossos models funcionam como um plugin do hapi que estamos exportando aqui
// Tratar routes, controllers e models como plugins é bastante útil pois torna o código modular e nos permite passar
// parâmetros de configuração para estes plugins através dos nossos arquivos .json de configuraçao
// Este código carrega todos os models e os expõe como métodos do plugin

var Glob = require('glob');
var Path = require('path');
var Database = require('database');

var loadFiles = function (callback) {

    Glob(__dirname + Path.sep + '**' + Path.sep + 'model.js', function (err, files) {

        if (err) {
            return callback(err);
        }

        if (!files) {
            return callback(new Error('Models: No model found'));
        }

        var models = {};

        files.forEach(function (file) {

            var dirName = Path.dirname(file);
            var module = require(dirName);
            models[module.config.modelName] = module.model();
        });

        return callback(null, models);
    });
};

exports.register = function (server, options, next) {

    loadFiles(function (err, modelModules) {

        if (err) {
            server.log(['error', 'models'], err.message);
            return next(err);
        }

        Database.init(modelModules, function (err, models) {

            if (err) {
                server.log(['error', 'models'], err.message);
                return next(err);
            }

            server.bind({
                models: models
            });
            server.expose('models', models);
            next();
        });
    });
};

exports.register.attributes = {
    name: 'models',
    version: require('../package.json').version
};
