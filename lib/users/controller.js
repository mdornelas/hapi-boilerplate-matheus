var Joi = require('joi');
var Validation = require('validation');
var Errors = require('common-errors');

module.exports.findById = {
    handler: function (request, reply) {

        var User = this.models.User;
        var id = request.params.id;
        User.findById(id).then(function (user) {

            if (user) {
                return reply(user);
            }

            var err = new Errors.NotFoundError('User');
            err.message = 'Este usuário não foi encontrado';
            reply(err);
        }).catch(reply);
    },

    validate: {
        params: Joi.object().keys({
            id: Validation.validators.User.id.required()
        })
    }
};

module.exports.findAll = {
    handler: function (request, reply) {

        var User = this.models.User;
        var Device = this.models.Device;
        User.findAll({
            include: [{
                model: Device // Inclui nos resultados da query os devices do usuário
            }]
        }).then(function (users) {

            reply(users);
        }).catch(reply);
    }
};

module.exports.create = {
    handler: function (request, reply) {

        var User = this.models.User;
        var Device = this.models.Device;
        var deviceId = request.payload.deviceId;
        var user = null;
        User.create(request.payload).then(function (_user) {

            user = _user;
            /*return Device.findById(deviceId);
        }).then(function (device) {

            if (device) {
                return user.addDevice(device);
            }*/
        }).then(function () {

            reply(user).code(201); // HTTP Status 201 - CREATED
        }).catch(reply);
    },

    validate: {
        payload: Joi.object().keys({
            name: Validation.validators.User.name.required(),
            email: Validation.validators.User.email.required(),
            password: Validation.validators.User.password.required(),
            city: Validation.validators.User.city.required(),
            phone: Validation.validators.User.phone.required()
        })
    }
};
