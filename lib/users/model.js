var Bcrypt = require('bcrypt');

var normalizeEmail = require('validation').normalizeEmail;

exports.schema = function (DataTypes) {

    return {
        id: {
            type: DataTypes.UUID,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
            uniqueMessage: 'O e-mail informado já está cadastrado'
        },
        password: {
            type: DataTypes.STRING
        },
        city: {
            type: DataTypes.STRING
        },
        phone: {
            type: DataTypes.INTEGER
        }
    };
};

exports.associate = function (models) {

    this.hasMany(models.Device);
    this.hasOne(models.Account, {
        foreignKey: {
            constraints: false,
            unique: true
        }
    });
};

exports.classMethods = { // http://docs.sequelizejs.com/en/latest/docs/models-definition/#expansion-of-models

};

exports.instanceMethods = { // http://docs.sequelizejs.com/en/latest/docs/models-definition/#expansion-of-models
    passwordHash: function (callback) {

        Bcrypt.hash(this.password, 10, function (err, hash) {

            if (err) {
                return callback(err);
            }

            this.password = hash;
            callback(null, hash);
        });
    },

    passwordVerify: function (password, callback) {

        Bcrypt.compare(password, this.password, callback);
    },

    toJSON: function () {

        var values = this.get();
        delete values.password;
        return values;
    }
};

exports.hooks = { // Hooks nos permitem tomar ações dentro do ciclo de vida do model: http://docs.sequelizejs.com/en/latest/docs/hooks/
    beforeCreate: function (instance, options, next) {

        // Fazemos alguns 'ajustes' nos dados antes de salvá-los, como por exemplo remover espaços em branco no inicio e no fim, tirar maiusculas do email, etc.
        instance.email = normalizeEmail(instance.email.trim());
        instance.name = instance.name.trim();

        // Substitui o campo 'password' pela hash antes de salvar
        instance.passwordHash(function (err, hash) {

            if (err) {
                return next(err);
            }

            instance.password = hash;
            next();
        });
    }
};
