var Joi = require('joi');

var isFullName = function (val) {

    if (val.trim().split(' ').length > 1) {
        return true;
    }

    return false;
};

module.exports = function (Generic) {

    return {
        id: Generic.id
            .meta({ message: 'Este usuário não foi encontrado' })
            .description('ID do usuário'),

        name: Joi.string().min(5).max(100).test(isFullName)
            .meta({ message: 'Informe o seu nome completo' })
            .description('Nome do usuário'),

        email: Generic.email
            .meta({ message: 'Verifique por erros de digitação no e-mail como espaços ou acentos' })
            .description('E-mail do usuário'),

        password: Generic.password
            .description('Senha do usuário'),

        city: Joi.string().valid(['Recife', 'Olinda', 'Jaboatão'])
            .meta({ message: 'A cidade informada não é permitida' })
            .description('Cidade do usuário'),

        phone: Joi.number().min(10000000).max(999999999)
            .meta({ message: 'Informe o seu telefone apenas com números' })
            .description('Telefone do usuário'),

        deviceId: Generic.id
            .meta({ message: 'Device não encontrado' })
            .description('ID do device para associar ao usuário')
    };
};
