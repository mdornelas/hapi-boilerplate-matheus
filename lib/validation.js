var Glob = require('glob');
var Path = require('path');
var Joi = require('joi');
var Validator = require('validator');

var generic = {
    id: Joi.string().length(22),

    email: Joi.string().max(100).test(Validator.isEmail)
        .meta({ message: 'O e-mail informado não é válido' }),

    password: Joi.string().min(5).max(100)
        .meta({ message: 'Informe uma senha com pelo menos 5 caracteres' })
};

exports.validators = (function () {

    var files = Glob.sync(__dirname + Path.sep + '**' + Path.sep + 'validator.js');

    var validators = {
        Generic: generic
    };

    files.forEach(function (file) {

        var dirName = Path.dirname(file);
        var module = require(dirName);
        var validator = module.validator(generic);
        validators[module.config.modelName] = validator;
    });

    return validators;
})();

exports.validationFunction = function (schema, key) {

    if (key) {
        var _schema = {};
        _schema[key] = schema;
        schema = _schema;
    }

    return function (val, callback) {

        if (key) {
            var _val = {};
            _val[key] = val;
            val = _val;
        }

        Joi.validate(val, schema, callback);
    };
};

exports.normalizeEmail = Validator.normalizeEmail;
