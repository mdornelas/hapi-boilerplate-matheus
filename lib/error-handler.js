// Módulo que exporta algumas funções para tratamento de erros
// Essas funções pegam o erro e formatam de maneira amigável para retornar na rest api

var Errors = require('common-errors');
var Boom = require('boom');
var _ = require('lodash');

// Função usada para tratar os erros gerados no controler quando se chama reply(err)
// Ela 'intercepta' o erro antes que ele seja enviado de fato, para transforma-los num objeto do 'Boom' que será retornado pra rest api
// Apenas formata o erro de maneira mais amigável e envia o resultado para a requisição pelo 'reply'
module.exports.response = function (request, reply) {

    var err = request.response;

    if (err instanceof Error === false) { // Não foi erro
        return reply.continue();
    }

    request.log(['error', 'error-handler'], 'Controller handler replied with error');
    request.log(['error', 'error-handler'], err);

    // Idealmente gostaríamos de lidar apenas com 'common-errors' na nossa aplicaçao para que o tratamento fique uniforme
    // Porem quando lançamos um erro do common-error na fase de validação do Sequelize, ele agrega esse erro em um array de erros dentro de um erro próprio
    // Aqui fazemos um tratamento para pegar o erro que nós criamos de dentro do erro criado pelo Sequelize, para podermos tratar na sequência
    if (err.name === 'SequelizeValidationError' && err.errors && err.errors[0].value instanceof Error) { // http://docs.sequelizejs.com/en/latest/api/errors/
        request.log(['error', 'error-handler'], 'Sequelize Validation Error');
        err = err.errors[0].value; // Pega o primeiro
    }

    // Aqui fazemos tratamento de qualquer outro erro do sequelize e deixamos ele genérico
    if (err.name.indexOf('Sequelize') === 0) {
        err = new Errors.data.SQLError('Sequelize Error: ' + err.name, err);
    }

    // Tratamento de erros operacionais (nao damos throw nesses erros pois eles são casos comuns do nosso codigo)
    var boom = null;
    if (err instanceof Errors.AlreadyInUseError) {
        request.log(['error', 'error-handler'], 'AlreadyInUseError');
        boom = Boom.conflict(err.message);
        boom.output.payload.developerMessage = err.generateMessage();
        boom.output.payload.keys = err.arg1;
        reply(boom);
    } else if (err instanceof Errors.NotFoundError) {
        request.log(['error', 'error-handler'], 'NotFoundError');
        boom = Boom.notFound(err.message);
        boom.output.payload.developerMessage = err.generateMessage();
        reply(boom);
    } // Abaixo tratamos erros inesperados (programmer error). Vamos retorar uma mensagem gerérica na rest e dar throw
    else if (err instanceof Errors.data.SQLError) {
        request.log(['error', 'error-handler'], 'SQLError');
        boom = Boom.wrap(err, 500);
        boom.output.payload.developerMessage = err.message;
        boom.output.payload.message = 'Ocorreu um erro no servidor. Por favor, tente novamente dentro de alguns instantes';
        reply(boom);
        throw boom;
    } else {
        request.log(['error', 'error-handler'], 'Unknown error');
        var status = (err.status && err.status >= 400) ? err.status : 500;
        boom = Boom.wrap(err, status);
        boom.output.payload.developerMessage = err.message;
        boom.output.payload.message = 'Ocorreu um erro no servidor. Por favor, tente novamente dentro de alguns instantes';
        reply(boom);
        throw boom;
    }
};

// Funçao que pré processa o erro em caso de erro de validação
// Funciona de maneira análoga à funçao que trata erro do response
module.exports.validation = function (request, reply, source, err) {

    // Se não for um erro do Boom ou nao for um ValidationError, nao tratamos
    if (typeof err.isBoom === 'undefined' || err.isBoom === false) {
        return reply.continue();
    }

    if (err.data === null || err.data.name !== 'ValidationError') {
        return reply.continue();
    }

    request.log(['error', 'validation'], 'Validation error occurred on controller');

    // A mensagem de erro padrao que o Joi gera nao é amigável para exibir ao usuario
    // Vamos procurar se foi informado uma mensagem no campo 'meta' de validacao do Joi e substituir a mensagem padrao pela informada
    err.output.payload.developerMessage = err.output.payload.message;
    err.output.payload.validation = (typeof err.output.payload.validation !== 'undefined') ? err.output.payload.validation : {};
    err.output.payload.validation.provided = [''];

    var path = err.data.details[0].path; //key que causou o erro
    err.output.payload.validation.provided = (typeof err.data._object[path] !== 'undefined') ? [err.data._object[path]] : [''];

    // Procura se consegue localizar o 'meta' do schema contendo alguma mensagem
    var i = -1;
    var src = null;
    if (this.payload) {
        i = _.findIndex(this.payload._inner.children, { key: path });
        src = this.payload;
    }

    if (i === -1 && this.params) {
        i = _.findIndex(this.params._inner.children, { key: path });
        src = this.params;
    }

    if (i === -1 && this.query) {
        i = _.findIndex(this.query._inner.children, { key: path });
        src = this.query;
    }

    if (i >= 0) {
        var meta = src._inner.children[i].schema._meta;
        var e = _.find(meta, 'message');
        if (e && e.message) {
            err.output.payload.message = e.message;
        }
    }

    reply(err);
};
