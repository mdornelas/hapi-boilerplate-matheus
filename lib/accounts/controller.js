var Joi = require('joi');
var Validation = require('validation');
var Errors = require('common-errors');

module.exports.findById = {
    handler: function (request, reply) {

        var User = this.models.User;
        var Account = this.models.Account;
        var id = request.params.id;

        Account.findById(id).then(function (account) {

            if (account) {
                reply(account);
            } else {
                var err = new Errors.NotFoundError('Account');
                err.message = 'Esta conta não foi encontrada';
                reply(err);
            }
        }).catch(reply);
    },

    validate: {
        params: Joi.object().keys({
            id: Validation.validators.Account.id.required()
        })
    }
};

module.exports.findAll = {
    handler: function (request, reply) {

        var Account = this.models.Account;

        Account.findAll().then(function (accounts) {

            reply(accounts);
        }).catch(reply);
    }
};

module.exports.create = {
    handler: function (request, reply, next) {

        var Account = this.models.Account;
        Account.create(request.payload).then(function (account) {

            reply(account).code(201); // HTTP Status 201 - CREATED
        }).catch(reply);
    },

    validate: {
        payload: Joi.object().keys({
            type: Validation.validators.Account.type.required(),
            UserId: Validation.validators.Account.userIdentification.required()
        })
    }
};
