var UuidBase62 = require('uuid-base62');

exports.schema = function (DataTypes) {

    return {
        objectId: { // Gerado no beforeCreate
            type: DataTypes.UUID,
            primaryKey: true
        },
        type: {
            type: DataTypes.STRING
        }
    };
};

exports.associate = function (models) {

    this.belongsTo(models.User, {
        foreignKey: {
            constraints: false,
            unique: true
        }
    });
};

exports.findById = function (id) {

    // Na rest api não vamos expor o id numérico do model no BD, vamos tabalhar com este id aleatório gerado: https://en.wikipedia.org/wiki/Universally_unique_identifier
    return this.findOne({
        where: {
            objectId: id
        }
    });

};

exports.instanceMethods = {
    toJSON: function () {

        var values = this.get();

        delete values.id;
        return values;
    }
};

exports.hooks = { // Hooks nos permitem tomar ações dentro do ciclo de vida do model: http://docs.sequelizejs.com/en/latest/docs/hooks/
    beforeCreate: function (instance, options, next) {

        // Antes de criar, adicionamos um campo objectId com um identificador aleatório no padrão 'uuid': https://en.wikipedia.org/wiki/Universally_unique_identifier
        // Convertemos ele para base62 em vez do formato original
        instance.objectId = UuidBase62.v4();
        next();
    }
};

