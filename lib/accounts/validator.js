var Validator = require('validator');
var Joi = require('joi');

module.exports = function (Generic) {

    return {
        id: Generic.id
        .meta({ message: 'Esta conta não foi encontrada' })
        .description('Conta'),

        userIdentification: Joi.string().min(1).max(22)
        .meta({ message: 'Esta conta não foi encontrada' })
        .description('Conta'),

        type: Joi.string().min(2).max(100)
        .meta({ message: 'Informe o tipo de conta' })
        .description('Tipo de conta')
    };
};
