// Todos os nossos controllers funcionam como um plugin do hapi que estamos exportando aqui
// Tratar routes, controllers e models como plugins é bastante útil pois torna o código modular e nos permite passar
// parâmetros de configuração para estes plugins através dos nossos arquivos .json de configuraçao
// Este código carrega todos os handlers e os expõe como métodos do plugin

var Glob = require('glob');
var Path = require('path');
var ErrorHandler = require('error-handler');

var loadFiles = function (callback) {

    var controllers = {};

    Glob(__dirname + Path.sep + '**' + Path.sep + 'model.js', function (err, files) {

        if (err) {
            return callback(err);
        }

        if (!files) {
            return callback(new Error('Controllers: No controller found'));
        }

        files.forEach(function (file) {

            var dirName = Path.dirname(file);
            var module = require(dirName);
            var controller = module.controller();

            // Adicionamos o método de failAction no validate de cada handler
            for (var method in controller) {

                controller[method].tags = ['api', module.config.collectionName];

                if (!controller[method].validate) {
                    controller[method].validate = {};
                }

                if (!controller[method].validate.failAction) {
                    controller[method].validate.failAction = ErrorHandler.validation;
                }
            }

            controllers[module.config.collectionName] = controller;
        });

        return callback(null, controllers);
    });
};

exports.register = function (server, options, next) {

    loadFiles(function (err, controllers) {

        if (err) {
            server.log(['error', 'controllers'], err.message);
            return next(err);
        }

        server.ext('onPostHandler', ErrorHandler.response);
        server.bind({
            controllers: controllers
        });
        server.expose('handlers', controllers);

        next();
    });
};

exports.register.attributes = {
    name: 'controllers',
    dependencies: ['models'],
    version: require('../package.json').version
};
