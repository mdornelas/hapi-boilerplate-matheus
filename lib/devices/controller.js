var Joi = require('joi');
var Validation = require('validation');
var Errors = require('common-errors');

module.exports.findById = {
    handler: function (request, reply) {

        var Device = this.models.Device;
        var id = request.params.id;
        Device.findById(id).then(function (device) {

            if (device) {
                return reply(device);
            }

            var err = new Errors.NotFoundError('Device');
            err.message = 'Este device não foi encontrado';
            reply(err);
        }).catch(reply);
    },

    validate: {
        params: Joi.object().keys({
            id: Validation.validators.Device.id.required()
        })
    }
};

module.exports.findAll = {
    handler: function (request, reply) {

        var Device = this.models.Device;
        Device.findAll().then(function (devices) {

            reply(devices);
        }).catch(reply);
    }
};

module.exports.create = {
    handler: function (request, reply, next) {

        var Device = this.models.Device;
        var User = this.models.User;

        Device.create(request.payload).then(function (device) {

            reply(device).code(201); // HTTP Status 201 - CREATEDcatch(reply);
        }).catch(reply);

    },

    validate: {
        payload: Joi.object().keys({
            manufacturer: Validation.validators.Device.manufacturer.required(),
            model: Validation.validators.Device.model.required(),
            UserId: Validation.validators.Device.userId.required()
        })
    }
};
