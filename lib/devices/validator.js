var Joi = require('joi');

module.exports = function (Generic) {

    return {
        id: Generic.id
            .meta({ message: 'Este aparelho não foi encontrado' })
            .description('ID do aparelho'),

        manufacturer: Joi.string().min(2).max(100)
            .meta({ message: 'Informe o fabricante do aparelho' })
            .description('Fabricante do aparelho'),

        model: Joi.string().min(1).max(100)
            .meta({ message: 'Informe o modelo do aparelho' })
            .description('Modelo do aparelho'),

        userId: Joi.string().min(1).max(22)
        .meta({ message: 'Informe o ID do dono' })
        .description('User')
    };
};
