exports.controller = function () {

    return require('./controller');
};

exports.model = function () {

    return require('./model');
};

exports.validator = function () {

    return require('./validator').apply(this, arguments);
};

exports.config = {
    modelName: 'Device',
    collectionName: 'Devices'
};
