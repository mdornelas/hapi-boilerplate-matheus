exports.schema = function (DataTypes) {

    return {
        id: {
            type: DataTypes.UUID,
            primaryKey: true
        },
        manufacturer: {
            type: DataTypes.STRING
        },
        model: {
            type: DataTypes.STRING
        }
    };
};

exports.associate = function (models) {

    this.belongsTo(models.User);
};

exports.classMethods = { // http://docs.sequelizejs.com/en/latest/docs/models-definition/#expansion-of-models

};

exports.instanceMethods = { // http://docs.sequelizejs.com/en/latest/docs/models-definition/#expansion-of-models

};

exports.hooks = { // Hooks nos permitem tomar ações dentro do ciclo de vida do model: http://docs.sequelizejs.com/en/latest/docs/hooks/

};
