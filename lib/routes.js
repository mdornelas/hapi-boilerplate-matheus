// O nosso arquivo de routes na verdade é um plugin do hapi que estamos exportando aqui
// Tratar routes, controllers e models como plugins é bastante útil pois torna o código modular e nos permite passar
// parâmetros de configuração para estes plugins através dos nossos arquivos .json de configuraçao

exports.register = function (server, options, next) {

    var Controllers = server.plugins.controllers.handlers;
    var Models = server.plugins.models.models;

    server.bind({
        models: Models,
        controllers: Controllers
    });

    // Routes da nossa aplicação
    // Convenção para nome de metodos controllers:
    // Os métodos do controller (create, findById, etc) devem ser, sempre que possível, os mesmos do model
    server.route([

        // Estra estrutura é só um exemplo
        { method: 'GET',  path: '/users', config: Controllers.Users.findAll },
        { method: 'GET',  path: '/users/{id}', config: Controllers.Users.findById },
        { method: 'POST',  path: '/users', config: Controllers.Users.create },

        { method: 'GET',  path: '/devices', config: Controllers.Devices.findAll },
        { method: 'GET',  path: '/devices/{id}', config: Controllers.Devices.findById },
        { method: 'POST',  path: '/devices', config: Controllers.Devices.create },

        { method: 'GET',  path: '/accounts', config: Controllers.Accounts.findAll },
        { method: 'GET',  path: '/accounts/{id}', config: Controllers.Accounts.findById },
        { method: 'POST',  path: '/accounts', config: Controllers.Accounts.create }
    ]);

    next();
};

exports.register.attributes = {
    name: 'routes',
    dependencies: ['controllers', 'models'],
    version: require('../package.json').version
};
