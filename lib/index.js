require('app-module-path').addPath(__dirname); // Nos permite usar require no nosso codigo tendo referencia base esta pasta, evitando coisas como require('../../../arquivo')
var Hapi = require('hapi');
var Config = require('config'); // Carrega as configurações do servidor localizados na pasta 'config'. O default é 'development'.
var Glue = require('glue'); // Usado para carregar plugins do hapi a partir de configuração no formato json

var hapiConfig = Config.get('hapi');

module.exports.init = function (callback) {

    // Carrega as configuraçoes do hapi (isso inclui todos plugins do arquivo de configuraçao)
    Glue.compose(hapiConfig, { relativeTo: __dirname }, callback);
};
