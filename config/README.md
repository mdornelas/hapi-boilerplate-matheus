# Config

Esta pasta cont�m a configura��o do servidor separada em ambientes.
A configura��o � carregada no index.js usando o plugin 'config': https://www.npmjs.com/package/config
Para mudar o ambiente setamos a vari�vel de ambiente do sistema operacional NODE_ENV para o ambiente desejado: 'development', 'production', 'test', etc.
No Windows para setar a vari�vel do ambiente usamos o comando 'setx %NODE_ENV% "valor"' e reiniciamos o CMD.
No Linux basta fazer 'NODE_ENV = 1 comando', e o 'comando' ser� executado com essa vari�vel de ambiente.
O comportamento padr�o quando setamos um ambiente � combinar o default.json com o JSON do ambiente definido.
Os dois JSONs ser�o combinados, sendo que em caso de conflito o do ambiente sobrescreve o que tiver no 'default'.