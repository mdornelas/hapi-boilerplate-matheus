var Code = require('code');
var Lab = require('lab');
var Joi = require('joi');
var Boom = require('boom');
var Util = require('../util');
var Errors = require('common-errors');

// Variaveis auxiliares de teste
var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var expect = Code.expect;

var ErrorHandler = require('../../lib/error-handler');

var validationHandler = ErrorHandler.validation;

var request = {
    log: function () {

    }
};

describe('ErrorHandler.validation', function () {

    it('Calls continue if error is not Boom', function (done) {

        var reply = {
            continue: function () {

                done();
            }
        };

        validationHandler(request, reply, {}, new Error('test'));
    });

    it('Calls continue if error is not ValidationError', function (done) {

        var reply = {
            continue: function () {

                done();
            }
        };

        var error = new Error('test');
        error.data = { name: 'SomeError' };

        validationHandler(request, reply, {}, error);
    });

    it('Logs when a ValidationError happens', function (done) {

        var first = true;
        var req = {
            log: function () {

                if (first) {
                    first = false;
                    done();
                }
            }
        };
        var reply = function (err) {

        };

        var schema = Joi.object().keys({
            name: Joi.string()
        });
        var result = Joi.validate({
            name: 123
        }, schema);

        var error = Boom.badRequest('Test message', result.error);

        validationHandler(req, reply, {}, error);
    });

    it('Validates payload and replies with status code 400 and provided message', function (done) {

        var req = {
            log: function () {

            },

            payload: {
                name: 123
            },
            params: {}
        };

        var schema = Joi.object().keys({
            name: Joi.string().meta({ message: 'RightMessage' })
        });
        var result = Joi.validate(req.payload, schema);
        var error = Boom.badRequest('WrongMessage', result.error);

        var reply = function (err) {

            expect(err.isBoom).to.be.equal(true);
            expect(err.output.statusCode).to.be.equal(400);
            expect(err.output.payload.message).to.be.equal('RightMessage');
            done();
        };

        validationHandler.call({ payload: schema }, req, reply, {}, error);
    });

    it('Validates params and replies with status code 400 and provided message', function (done) {

        var req = {
            log: function () {

            },

            params: {
                name: 123
            },
            payload: {}
        };

        var schema = Joi.object().keys({
            name: Joi.string().meta({ message: 'RightMessage' })
        });
        var result = Joi.validate(req.params, schema);
        var error = Boom.badRequest('WrongMessage', result.error);

        var reply = function (err) {

            expect(err.isBoom).to.be.equal(true);
            expect(err.output.statusCode).to.be.equal(400);
            expect(err.output.payload.message).to.be.equal('RightMessage');
            done();
        };

        validationHandler.call({ params: schema }, req, reply, {}, error);
    });

    it('Validates query and replies with status code 400 and provided message', function (done) {

        var req = {
            log: function () {

            },

            query: {
                name: 123
            },
            payload: {}
        };

        var schema = Joi.object().keys({
            name: Joi.string().meta({ message: 'RightMessage' })
        });
        var result = Joi.validate(req.query, schema);
        var error = Boom.badRequest('WrongMessage', result.error);

        var reply = function (err) {

            expect(err.isBoom).to.be.equal(true);
            expect(err.output.statusCode).to.be.equal(400);
            expect(err.output.payload.message).to.be.equal('RightMessage');
            done();
        };

        validationHandler.call({ query: schema }, req, reply, {}, error);
    });

    it('Returns the error message if not provided in meta', function (done) {

        var req = {
            log: function () {

            },

            params: {
                name: 123
            },
            payload: {}
        };

        var schema = Joi.object().keys({
            name: Joi.string()
        });
        var result = Joi.validate(req.params, schema);
        var error = Boom.badRequest('RightMessage', result.error);

        var reply = function (err) {

            expect(err.isBoom).to.be.equal(true);
            expect(err.output.statusCode).to.be.equal(400);
            expect(err.output.payload.message).to.be.equal('RightMessage');
            done();
        };

        validationHandler.call({ params: schema }, req, reply, {}, error);
    });

    it('Calls continue if not ValidationError', function (done) {

        var reply = {
            continue: function () {

                done();
            }
        };

        validationHandler(request, reply, {}, Boom.create(409, 'Test'));
    });

    it('Returns error when required parameters are not present', function (done) {

        var req = {
            log: function () {

            },

            params: {

            }
        };

        var schema = Joi.object().keys({
            name: Joi.string().required()
        });
        var result = Joi.validate(req.params, schema);
        var error = Boom.badRequest('RightMessage', result.error);

        var reply = function (err) {

            expect(err.isBoom).to.be.equal(true);
            expect(err.output.statusCode).to.be.equal(400);
            expect(err.output.payload.message).to.be.equal('RightMessage');
            done();
        };

        validationHandler.call({ params: schema }, req, reply, {}, error);
    });
});
