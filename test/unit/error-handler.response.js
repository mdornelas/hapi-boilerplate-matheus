var Code = require('code');
var Lab = require('lab');
var Joi = require('joi');
var Util = require('../util');
var Errors = require('common-errors');
var Sequelize = require('sequelize');

// Variaveis auxiliares de teste
var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var expect = Code.expect;

var ErrorHandler = require('../../lib/error-handler');

var responseHandler = ErrorHandler.response;

describe('ErrorHandler.response', function () {

    it('Calls continue if an error didn\'t happen', function (done) {

        var request = {
            response: null,
            log: function () {

            }
        };

        var reply = {
            continue: function () {

                done();
            }
        };

        responseHandler(request, reply);
    });

    it('Logs if an error happened', function (done) {

        var first = true;

        var request = {
            response: new Errors.AlreadyInUseError('Test', 'key'),
            log: function () {

                if (first) {
                    first = false;
                    done();
                }
            }
        };

        var reply = function (err) {

        };

        responseHandler(request, reply);
    });

    it('Reply with status 409 if error is AlreadyInUseError', function (done) {

        var request = {
            response: new Errors.AlreadyInUseError('Test', 'key'),
            log: function () {

            }
        };

        var reply = function (err) {

            expect(err.isBoom).to.equal(true);
            expect(err.output.statusCode).to.equal(409);
            done();
        };

        responseHandler(request, reply);
    });

    it('Handles SequelizeValidationError to get AlreadyInUseError inside it', function (done) {

        var error = new Error('Testing');
        error.name = 'SequelizeValidationError';
        error.errors = [{
            value: new Errors.AlreadyInUseError('Test', 'key')
        }];

        var request = {
            response: error,
            log: function () {

            }
        };

        var reply = function (err) {

            replied = true;
            expect(err.isBoom).to.equal(true);
            expect(err.output.statusCode).to.equal(409);
            done();
        };

        responseHandler(request, reply);
    });

    it('Treats SequelizeValidationError that has no errors inside it as SQLError', function (done) {

        var error = new Sequelize.ValidationError();
        delete error.errors;

        var request = {
            response: error,
            log: function () {

            }
        };

        var replied = false;
        var threw = false;

        var reply = function (err) {

            replied = true;
            expect(err.isBoom).to.equal(true);
            expect(err.output.statusCode).to.equal(500);
            if (threw) {
                done();
            }

        };

        try {

            responseHandler(request, reply);
        } catch (e) {

            threw = true;
            if (replied) {
                done();
            }
        }
    });

    it('Reply with status 404 if error is NotFoundError', function (done) {

        var request = {
            response: new Errors.NotFoundError('Test'),
            log: function () {

            }
        };

        var reply = function (err) {

            expect(err.isBoom).to.equal(true);
            expect(err.output.statusCode).to.equal(404);
            done();
        };

        responseHandler(request, reply);
    });

    it('Reply with status 500 if error is SQLError and throw an exception', function (done) {

        var request = {
            response: new Errors.data.SQLError('Testing'),
            log: function () {

            }
        };

        var replied = false;
        var threw = false;

        var reply = function (err) {

            replied = true;
            expect(err.isBoom).to.equal(true);
            expect(err.output.statusCode).to.equal(500);
            if (threw) {
                done();
            }

        };

        try {

            responseHandler(request, reply);
        } catch (e) {

            threw = true;
            if (replied) {
                done();
            }
        }
    });

    it('Handles other Sequelize errors as SQLError and throws exception', function (done) {

        var error = new Error('Testing');
        error.name = 'SequelizeGenericError';

        var request = {
            response: error,
            log: function () {

            }
        };

        var replied = false;
        var threw = false;

        var reply = function (err) {

            replied = true;
            expect(err.isBoom).to.equal(true);
            expect(err.output.statusCode).to.equal(500);
            expect(err instanceof Errors.data.SQLError).to.equal(true);
            if (threw) {
                done();
            }

        };

        try {

            responseHandler(request, reply);
        } catch (err) {

            threw = true;
            if (replied) {
                done();
            }
        }
    });

    it('Reply with status 500 if error is generic and throw an exception', function (done) {

        var request = {
            response: new Error('Testing'),
            log: function () {

            }
        };

        var replied = false;
        var threw = false;

        var reply = function (err) {

            replied = true;
            expect(err.isBoom).to.equal(true);
            expect(err.output.statusCode).to.equal(500);
            if (threw) {
                done();
            }

        };

        try {

            responseHandler(request, reply);
        } catch (err) {

            threw = true;
            if (replied) {
                done();
            }
        }
    });

    it('Reply with error status if passed', function (done) {

        var error = new Error('Testing');
        error.status = 500;
        var request = {
            response: error,
            log: function () {

            }
        };

        var replied = false;
        var threw = false;

        var reply = function (err) {

            replied = true;
            expect(err.isBoom).to.equal(true);
            expect(err.output.statusCode).to.equal(error.status);
            if (threw) {
                done();
            }

        };

        try {

            responseHandler(request, reply);
        } catch (err) {

            threw = true;
            if (replied) {
                done();
            }
        }
    });

});
