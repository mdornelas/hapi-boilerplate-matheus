var Joi = require('joi');
var Validation = require('../../lib/validation');

var accountSchema = Validation.validators.Account;

module.exports = {
	create: {
		request: {
			method: 'POST',
			url: '/accounts',
			payload: {
				type: 'dev',
				UserId: '2vEUq5hr4SxnLGafFkytRU'
			}
		},
		status: 201,
			response: Joi.object().keys({
				type: Joi.string().valid('dev'),
				UserId: Joi.string().valid('2vEUq5hr4SxnLGafFkytRU'),
				objectId: accountSchema.id,
				updatedAt: Joi.date().iso(),
            	createdAt: Joi.date().iso()
			})
	},
	get: {
		request: function(id){
			return {
				method: 'GET',
				url: '/accounts/' + id
			};
		},
		status: 200,
		response: Joi.object().keys({
			objectId: accountSchema.id,
			type: accountSchema.type,
			UserId: accountSchema.userIdentification,
			updatedAt: Joi.date().iso(),
            createdAt: Joi.date().iso()
		})
	},

    getNotFound: {
        request: {
            method: 'GET',
            url: '/accounts/xxxxxxxxxxxxxxxxxxxxxx'
        },

        status: 404,
        response: Joi.object().keys({
            statusCode: Joi.number().valid(404),
            error: Joi.string().valid('Not Found'),
            message: Joi.string(),
            developerMessage: Joi.string()
        })
    },

    getAll: {
        request: {
            method: 'GET',
            url: '/users'
        },
        status: 200,
        response: Joi.array().items(
            Joi.object().keys({
				objectId: accountSchema.id,
				type: accountSchema.type,
				UserId: accountSchema.userIdentification,
				updatedAt: Joi.date().iso(),
	            createdAt: Joi.date().iso()
            })
        ).min(1)
    }    	
};