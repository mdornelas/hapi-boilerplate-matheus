var Code = require('code');
var Lab = require('lab');
var Joi = require('joi');
var Util = require('../util');

// Variaveis auxiliares de teste
var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var expect = Code.expect;

// Remove o BD
//Util.databaseRemove();

// Importa o servidor
var app = require('../../lib');
var server = null;

// A cada teste inicia o servidor novamente
lab.beforeEach(function (done) {

    app.init(function (err, serv) {

        expect(err).to.not.exist();
        expect(serv).to.exist();

        server = serv;
        done();
    });
});

// Importa os dados do teste
var testData = require('./users.data.js');
var userId = '';

describe('Users', function () {

    it('can be created', function (done) {

        var data = testData.create;
        server.inject(data.request, function (response) {

            expect(response.statusCode).to.equal(data.status);

            Joi.validate(response.payload, data.response, function (err) {

                expect(err).to.equal(null);

                userId = response.result.id;
                done();
            });
        });
    });

/*    it('can be created and associated with device', function (done) {

        var data = testData.createWithDeviceId;

        // Cria o device
        server.inject(data.requestDevices, function (firstResponse) {

            expect(firstResponse.statusCode).to.equal(data.status);
            var deviceId = firstResponse.result.id;

            // Agora com o deviceId, criamos o usuário passando o deviceId para ser associado
            server.inject(data.requestUsers(deviceId), function (secondResponse) {

                expect(secondResponse.statusCode).to.equal(data.status);
                Joi.validate(secondResponse.payload, data.response, function (err) {

                    expect(err).to.equal(null);
                    done();
                });
            });
        });
    });*/

    it('can\'t be created with invalid email', function (done) {

        var data = testData.createInvalidEmail;
        server.inject(data.request, function (response) {

            expect(response.statusCode).to.equal(data.status);
            Joi.validate(response.payload, data.response, function (err) {

                expect(err).to.equal(null);
                done();
            });
        });
    });

    it('can\'t be created with first name only', function (done) {

        var data = testData.createFirstNameOnly;

        server.inject(data.request, function (response) {

            expect(response.statusCode).to.equal(data.status);
            Joi.validate(response.payload, data.response, function (err) {

                expect(err).to.equal(null);
                done();
            });
        });
    });

    it('can\'t be created with an email that already exists', function (done) {

        var data = testData.createDuplicateEmail;
        server.inject(data.request, function (response) {

            expect(response.statusCode).to.equal(data.status);
            Joi.validate(response.payload, data.response, function (err) {

                expect(err).to.equal(null);
                done();
            });
        });
    });

    it('can be retrieved by id', function (done) {

        var data = testData.get;
        server.inject(data.request(userId), function (response) {

            expect(response.statusCode).to.equal(data.status);
            Joi.validate(response.payload, data.response, function (err) {
                expect(err).to.equal(null);
                done();
            });
        });
    });

    it('can\'t be retrieved by inexistent id', function (done) {

        var data = testData.getNotFound;
        server.inject(data.request, function (response) {

            expect(response.statusCode).to.equal(data.status);
            Joi.validate(response.payload, data.response, function (err) {

                expect(err).to.equal(null);
                done();
            });
        });
    });

    it('can be listed', function (done) {

        var data = testData.getAll;
        server.inject(data.request, function (response) {

            expect(response.statusCode).to.equal(data.status);
            Joi.validate(response.payload, data.response, function (err) {

                expect(err).to.equal(null);
                done();
            });
        });
    });
});
