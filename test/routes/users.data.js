var Joi = require('joi');
var Validation = require('../../lib/validation');
var Util = require('../util');

var userSchema = Validation.validators.User;

module.exports = {
    create: {
        request: {
            method: 'POST',
            url: '/users',
            payload: {
                name: 'David Benko',
                email: 'david@artics.com.br',
                password: '12345',
                city: 'Recife',
                phone: 88888888
            }
        },
        status: 201,
        response: Joi.object().keys({
            id: userSchema.id,
            name: Joi.string().valid('David Benko'),
            email: Joi.string().valid('david@artics.com.br'),
            city: Joi.string().valid('Recife'),
            phone: Joi.number().valid(88888888),
            updatedAt: Joi.date().iso(),
            createdAt: Joi.date().iso()
        })
    },

/*    createWithDeviceId: {
        requestDevices: {
            method: 'POST',
            url: '/devices',
            payload: {
                manufacturer: 'Samsung',
                model: 'Galaxy S6'
            }
        },
        requestUsers: function (deviceId) {

            return {
                method: 'POST',
                url: '/users',
                payload: {
                    name: 'David Benko',
                    email: 'david2@artics.com.br',
                    password: '12345',
                    city: 'Recife',
                    phone: 88888888,
                    deviceId: deviceId
                }
            };
        },

        status: 201,
        response: Joi.object().keys({
            id: userSchema.id,
            name: Joi.string().valid('David Benko'),
            email: Joi.string().valid('david2@artics.com.br'),
            city: Joi.string().valid('Recife'),
            phone: Joi.number().valid(88888888),
            updatedAt: Joi.date().iso(),
            createdAt: Joi.date().iso()
        })
    },*/

    createInvalidEmail: {
        request: {
            method: 'POST',
            url: '/users',
            payload: {
                name: 'David Benko',
                email: 'david @artics.com.br',
                password: '12345',
                city: 'Recife',
                phone: 88888888
            }
        },
        status: 400,
        response: Joi.object().keys({
            statusCode: Joi.number().valid(400),
            error: Joi.string().valid('Bad Request'),
            message: Joi.string(),
            developerMessage: Joi.string(),
            validation: Joi.object().keys({
                source: Joi.string().valid('payload'),
                keys: Joi.array().items(
                    Joi.string().valid('email')
                ),
                provided: Joi.array().items(
                    Joi.string().valid('david @artics.com.br')
                )
            })
        })
    },

    createFirstNameOnly: {
        request: {
            method: 'POST',
            url: '/users',
            payload: {
                name: 'David',
                email: 'david@artics.com.br',
                password: '12345',
                city: 'Recife',
                phone: 88888888
            }
        },
        status: 400,
        response: Joi.object().keys({
            statusCode: Joi.number().valid(400),
            error: Joi.string().valid('Bad Request'),
            message: Joi.string(),
            developerMessage: Joi.string(),
            validation: Joi.object().keys({
                source: Joi.string().valid('payload'),
                keys: Joi.array().items(
                    Joi.string().valid('name')
                ),
                provided: Joi.array().items(
                    Joi.string().valid('David')
                )
            })
        })
    },

    createDuplicateEmail: {
        request: {
            method: 'POST',
            url: '/users',
            payload: {
                name: 'David Benko Iseppon',
                email: 'david@artics.com.br',
                password: '12345',
                city: 'Olinda',
                phone: 988888888
            }
        },
        status: 409,
        response: Joi.object().keys({
            statusCode: Joi.number().valid(409),
            error: Joi.string().valid('Conflict'),
            message: Joi.string(),
            developerMessage: Joi.string(),
            keys: Joi.string().valid('email')
        })
    },

    get: {
        request: function (id) {

            return	{
                method: 'GET',
                url: '/users/' + id
            };
        },

        status: 200,
        response: Joi.object().keys({
            id: userSchema.id,
            name: Joi.string().valid('David Benko'),
            email: Joi.string().valid('david@artics.com.br'),
            city: Joi.string().valid('Recife'),
            phone: Joi.number().valid(88888888),
            updatedAt: Joi.date().iso(),
            createdAt: Joi.date().iso()
        })
    },

    getNotFound: {
        request: {
            method: 'GET',
            url: '/users/xxxxxxxxxxxxxxxxxxxxxx'
        },

        status: 404,
        response: Joi.object().keys({
            statusCode: Joi.number().valid(404),
            error: Joi.string().valid('Not Found'),
            message: Joi.string(),
            developerMessage: Joi.string()
        })
    },

    getAll: {
        request: {
            method: 'GET',
            url: '/users'
        },
        status: 200,
        response: Joi.array().items(
            Joi.object().keys({
                id: userSchema.id,
                name: userSchema.name,
                email: userSchema.email,
                city: userSchema.city,
                phone: userSchema.phone,
                Devices: Joi.array(),
                updatedAt: Joi.date().iso(),
                createdAt: Joi.date().iso()
            })
        ).min(1)
    }
};
