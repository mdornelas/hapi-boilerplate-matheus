var Joi = require('joi');
var Validation = require('../../lib/validation');

var deviceSchema = Validation.validators.Device;

module.exports = {
    create: {
        request: {
            method: 'POST',
            url: '/devices',
            payload: {
                manufacturer: 'Samsung',
                model: 'Galaxy S6',
                UserId: '2vEUq5hr4SxnLGafFkytRU'
            }
        },
        status: 201,
        response: Joi.object().keys({
            id: deviceSchema.id,
            manufacturer: Joi.string().valid('Samsung'),
            model: Joi.string().valid('Galaxy S6'),
            updatedAt: Joi.date().iso(),
            createdAt: Joi.date().iso(),
            UserId: Joi.string().valid('2vEUq5hr4SxnLGafFkytRU')
        }),
    },

    get: {
        request: function (id) {

            return	{
                method: 'GET',
                url: '/devices/' + id
            };
        },

        status: 200,
        response: Joi.object().keys({
            id: deviceSchema.id,
            manufacturer: Joi.string().valid('Samsung'),
            model: Joi.string().valid('Galaxy S6'),
            updatedAt: Joi.date().iso(),
            createdAt: Joi.date().iso(),
            UserId: Joi.string().valid('2vEUq5hr4SxnLGafFkytRU')
        })
    },

    getNotFound: {
        request: {
            method: 'GET',
            url: '/devices/xxxxxxxxxxxxxxxxxxxxxx'
        },

        status: 404,
        response: Joi.object().keys({
            statusCode: Joi.number().valid(404),
            error: Joi.string().valid('Not Found'),
            message: Joi.string(),
            developerMessage: Joi.string()
        })
    },

    getAll: {
        request: {
            method: 'GET',
            url: '/devices'
        },
        status: 200,
        response: Joi.array().items(
            Joi.object().keys({
                id: deviceSchema.id,
                manufacturer: deviceSchema.manufacturer,
                model: deviceSchema.model,
                updatedAt: Joi.date().iso(),
                createdAt: Joi.date().iso(),
                UserId: deviceSchema.userId
            })
        ).min(1)
    }
};
