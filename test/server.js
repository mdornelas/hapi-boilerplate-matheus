var Code = require('code');
var Lab = require('lab');

var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var expect = Code.expect;

var app = require('../lib');

describe('Server', function () {

    it('can be initialized and started', function (done) {

        app.init(function (err, server) {

            expect(err).to.not.exist();
            expect(server).to.exist();

            server.start(function () {

                expect(server.info.uri).to.exist();
                done();
            });
        });
    });
});
