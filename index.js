// Nosso código de lógica do servidor fica todo na pasta lib
var app = require('./lib');

app.init(function (err, server) {

    if (err) {
        console.log('-> Error initializing server:', err);
        return;
    }

    server.start(function () {

        console.log('-> Server is listening on ' + server.info.uri.toLowerCase());
    });
});
