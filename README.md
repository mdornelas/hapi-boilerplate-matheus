# Hapi Boilerplate

Estrutura de projeto para o Hapi

## Comandos

* **npm run start**: Apenas roda o servidor
* **npm run monitor**: Roda o servidor monitorando por mudanças e renicia caso aconteçam
* **npm run debug**: Roda o servidor no modo de depuração
* **npm test**: Roda os testes da pasta 'test'
* **npm run lint**: Formata o código usando JSCS no padrão definido, verifica erros de programação com ESHint e depois roda os testes
* **npm run coverage**: Roda os testes da pasta 'test' gerando relatório de code coverage

## Dependências

### Production dependencies:

* **app-module-path**: Permite dar require em arquivos nossos com relação ao root do projeto. Evita coisas do tipo `require('../../../arquivo')`;
* **bcrypt**: Gera hashs no formato bcrypt. Utilizado para as senhas.
* **blipp**: Mostra as routes da nossa aplicação ao rodar o servidor
* **boom**: Cria objetos da classe Error prontos para ser retornados na requisição HTTP
* **common-errors**: Agrega diversos tipos de erro para que possamos utilizar em qualquer lugar do código sem precisar definir um tipo novo
* **config**: Gerencia ambientes de produção/desenvolvimento/testes baseado em arquivos JSON e variáveis de ambiente
* **glob**: Usado para listar todos arquivos de uma determinada extensão numa pasta. Exemplo `glob("**/*.js", function(err, files) { ... });`
* **glue**: Usado para iniciar o servidor a partir de um objeto de configuração
* **good**: Gerenciador de eventos que acontecem no servidor (requests, erros, etc)
* **good-file**: Reporter do good. Pega os dados do good e salva em arquivo
* **hapi**: Framework principal
* **joi**: Validação de objetos
* **lodash**: Biblioteca com muitas utilidades (derivada do underscore, chamamos ela de \_). Permite programar de maneira que lembra funcional: `_.map([1, 2, 3], function(n) { return n * 3; }); // resultado: [3, 6, 9]`
* **mysql**: Adapter para conectar ao mysql
* **poop**: Monitora uncaught exceptions no nosso código e salva num arquivo
* **sequelize**: ORM para abstrair a comunicação com o BD
* **uuid-base62**: Usado para gerar identificadores únicos no padrão universal UUID, porém em base62 (0-9,a-z,A-Z) pois fica mais amigável na url
* **validator**: Biblioteca com muitas funções de validaçao úteis (email, telefone, cartão de crédito, etc)

### Development dependencies:

* **code**: Biblioteca de assertion feita para o hapi derivada do chai
* **good-console**: Reporter do good. Pega os dados do good e mostra no console
* **hapi-swagger**: Gera o api explorer do swagger, acessível em /explorer
* **inert**: Handler para routes que serve arquivos estáticos (usado pelo hapi-swagger)
* **jscs**: Formata o código de acordo com regras de formatação configuráveis
* **lab**: Biblioteca de testes feita para o hapi derivada do mocha
* **node-inspector**: Utilizada para debugar o código usando breakpoints, vendo variáveis, etc
* **nodemon**: Quando utilizado para rodar o projeto, verifica por modificações no arquivo e reinicia o servidor
* **sqlite3**: Adapter para conectar ao sqlite (usado apenas em desenvolvimento)
* **vision**: Usado para trabalhar com views para renderizar templates html (usado pelo hapi-swagger)

## Request life cycle

Muito útil saber a ordem que as coisas acontecem no Hapi desde que uma requisição chega até ela ser retornada para o cliente

![Hapi request life cycle](http://s7.postimg.org/bxc21acln/hapi_request_lifecycle.png "Hapi request life cycle")